#Custom Docker images

Images for PHP and Nginx 

Docker installation is required, please visit the [official installation docs](https://docs.docker.com/).


## Steps for running this image

#Create the folder that will contain the images
```
mkdir ~/docker
mkdir ~/docker/{nginx,php}
```
##Run the image for nginx
```
docker build -f nginx/Dockerfile -t docker/nginx nginx
```
** Remember ** to copy the conf files from the corresponding folder

##Image for PHP 

# Remember to create the two configuration files: php-fpm.conf and www.conf
```
docker build -f php/Dockerfile -t docker/php php
```




